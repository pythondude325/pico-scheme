#include <string_view>
#include <string>
#include <vector>
#include <cstdio>
#include "pico/stdlib.h"
#include <fmt/format.h>

using std::string;
using std::string_view;
using std::vector;

enum TokenType {
    LIST_START,
    VECTOR_START,
    BYTEVECTOR_START,
    LIST_END,

    QUOTE,
    QUASIQUOTE,
    UNQUOTE,
    UNQUOTE_SPLICING,
    DOT,

    IDENTIFIER,
    LITERAL,
    EOF_TOKEN,

    WHITESPACE,

    SEXPR_COMMENT,
};

struct Token {
    TokenType type;
    string_view source;
    Token(TokenType type, std::string_view source) : type(type), source(source) {}
};

template <>
struct fmt::formatter<Token> : fmt::formatter<string_view> {
    template<typename FormatContext>
    auto format(const Token &t, FormatContext &ctx){
        return fmt::format_to(ctx.out(), "<Token {};{}>", t.type, t.source);
    }
};

void scanToken(string_view source, vector<Token> &tokens) {
    if (source.length() == 0) {
        tokens.push_back(Token(EOF_TOKEN, source));
        return;
    }

    char c = source[0];
    auto first_char_substr = source.substr(0, 1);

    switch (c) {
        case '(':
            tokens.push_back(Token(LIST_START, first_char_substr));
            break;
        case ')':
            tokens.push_back(Token(LIST_END, first_char_substr));
            break;
        case '.':
            tokens.push_back(Token(DOT, first_char_substr));
            break;
        case '\'':
            tokens.push_back(Token(QUOTE, first_char_substr));
            break;
        case '`':
            tokens.push_back(Token(QUASIQUOTE, first_char_substr));
            break;
        case ',':
            tokens.push_back(Token(UNQUOTE_SPLICING, first_char_substr));
            break;
        case ' ':
        case '\t':
        case '\n':
            tokens.push_back(Token(WHITESPACE, first_char_substr));
            break;
        case '#':
            string_view rest = source.substr(1);
            if(rest.starts_with("(")){
                
            } else if(rest.starts_with("u8(")) {

            } else if(rest.starts_with("\\")) {

            } else if(rest.starts_with(";")){

            }
            break;
    }

    return scanToken(source.substr(1, 1), tokens);
}

std::vector<Token> scanTokens(std::string_view source) {
    std::vector<Token> tokens;
    scanToken(source, tokens);
    return tokens;
}

void print(string_view str, FILE *output){
    fwrite(str.data(), 1, str.length(), output);
    fflush(output);
}

void getline(FILE *input, string &buffer, char delim = '\r') {
    buffer.clear();
    while(true){
        int c = fgetc(input);
        if(c != -1){
            buffer.push_back(static_cast<char>(c));
        }
        if(c == '\n' || c == '\r'){
            break;
        }
    }
}

void turn_on_led() {
    const uint LED_PIN = 25;
    gpio_init(LED_PIN);
    gpio_set_dir(LED_PIN, GPIO_OUT);
    gpio_put(LED_PIN, 1);
}

int main() {
    stdio_init_all();

    sleep_ms(3000);
    turn_on_led();

    std::string buff;

    while (true) {
        print("> ", stdout);
        getline(stdin, buff);
        buff.pop_back();

        Token my_token(LITERAL, buff);

        fmt::print(stdout, "newline: {:x}\n", buff[buff.length() - 1]);
        fmt::print(stdout, "Token: {}\n", my_token);
    }
}
